from django.apps import AppConfig


class TwitterscrapperConfig(AppConfig):
    name = 'twitterscrapper'

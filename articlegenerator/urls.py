from . import views
from django.urls import include, path
from django.conf.urls import url

from articlegenerator.views import *
urlpatterns = [
    path('list', GeneratedArticleList, name='article-list'),
    path('add', ArticleGenerator, name='article-add'),
    #path('read', ArticleRead, name='article-read'),
    url(r'^read/(?P<id>\w{0,50})/$', ArticleRead,name='article-read'),
    path('generate-article', RenderCsvFile, name='generate-article'),
    
]
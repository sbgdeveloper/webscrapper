from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import get_object_or_404, redirect, render
from wikiscrapper.models import *
import csv
from io import StringIO
from .models import *

def GeneratedArticleList(request):
	articles = PostWpPost.objects.all()
	context={
		'articles':articles,
	}
	return render(request, "articlegenerator/templates/list.html", context)

def ArticleGenerator(request):
	
	files = FilesModel.objects.all()
	context={
		'files':files,
	}
	return render(request, "articlegenerator/templates/add.html", context)

def ArticleRead(request,id):
	article = WpPostModel.objects.filter(id=id).first()
	context={
		'article':article,
	}
	return render(request, "articlegenerator/templates/read.html", context)	



def RenderCsvFile(request):
	if request.method == 'POST':
		csv_file_path = request.POST.get('file_path')
		#csv_file_path = "/var/www/html/webscrapper/storage/wikipedia/20200626.csv"
		context={}
		lines=[]
		with open(csv_file_path, 'r') as csv_file:
			csv_reader = csv.reader(csv_file)
			i = next(csv_reader)
			rest = [row for row in csv_reader]
			for row in rest:
				keyword = row[0]
				pagetitle = row[1] 
				content = row[2] 
				image = row[3] 
				
				data = PostWpPost()
				data.website_id = 1
				data.keyword_category = 0
				data.keywords = keyword
				data.post_content = content
				data.post_title = pagetitle
				data.image = image
				data.post_status = "Draft"
				data.post_author = 1
				data.save()

	return redirect('/')
	
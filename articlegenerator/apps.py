from django.apps import AppConfig


class ArticlegeneratorConfig(AppConfig):
    name = 'articlegenerator'

import arrow
import binascii
import datetime
import os
import time
from django.core.cache import cache
from django.db import models
from django.utils.translation import ugettext_lazy as _
# Create your models here.


class WpPostModel(models.Model):

    id = models.IntegerField(primary_key=True)
    post_author = models.IntegerField()
    post_date = models.DateTimeField(_("Created on"), auto_now_add=True)
    keywords = models.TextField()
    keyword_category = models.IntegerField()
    tags = models.TextField()
    post_date_gmt = models.DateTimeField(_("Created on"), auto_now_add=True)
    post_content = models.TextField()
    post_title = models.TextField()
    post_status = models.TextField()
    comment_status = models.TextField()
    ping_status = models.TextField()
    post_password = models.TextField()
    post_name = models.TextField()
    to_ping = models.TextField()
    pinged = models.TextField()
    post_modified = models.DateTimeField(_("Created on"), auto_now_add=True)
    post_modified_gmt = models.DateTimeField(_("Created on"), auto_now_add=True)
    post_content_filtered = models.TextField()
    post_parent = models.IntegerField()
    guid = models.TextField()
    menu_order = models.IntegerField()
    post_type = models.TextField()
    post_mime_type = models.TextField()
    comment_count = models.IntegerField()
    website_id = models.IntegerField()
    image = models.TextField()
    created_at = models.DateField(_("Created on"), auto_now_add=True)
    
    class Meta:
        db_table = "wp_posts"
        def __str__(self):
            return self 


class PostWpPost(models.Model):

    id = models.IntegerField(primary_key=True)
    post_author = models.IntegerField(blank=True)
    post_date = models.DateTimeField(_("Created on"), auto_now_add=True)
    keywords = models.TextField()
    keyword_category = models.IntegerField(blank=True)
    post_date_gmt = models.DateTimeField(_("Created on"), auto_now_add=True)
    post_content = models.TextField()
    post_title = models.TextField()
    post_status = models.TextField()
    post_modified = models.DateTimeField(_("Created on"), auto_now_add=True)
    post_modified_gmt = models.DateTimeField(_("Created on"), auto_now_add=True)
    website_id = models.IntegerField()
    image = models.TextField(blank=True)
    created_at = models.DateField(_("Created on"), auto_now_add=True)
    
    class Meta:
        db_table = "wp_posts"
        def __str__(self):
            return self
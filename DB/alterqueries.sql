-- 26-6-2020

CREATE TABLE `webscrapperdb`.`websites` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NULL,
  `url` VARCHAR(100) NULL,
  `status` INT NULL DEFAULT 1 COMMENT '0=inactive 1=active',
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  `created_by` INT NULL,
  `updated_by` INT NULL,
  `deleted_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`));


  CREATE TABLE `webscrapperdb`.`files` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `website_id` INT NULL,
  `filename` VARCHAR(350) NULL,
  `status` INT NULL DEFAULT 0 COMMENT '0=unpublished 1=published',
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  `created_by` INT NULL,
  `updated_by` INT NULL,
  `deleted_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`));


  -- 29-06-2020--------
  CREATE TABLE `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '1',
  `post_date` datetime NOT NULL,
  `keywords` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `keyword_category` tinyint(2) DEFAULT '0',
  `tags` text CHARACTER SET utf8,
  `post_date_gmt` datetime NOT NULL,
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci,
  `pinged` text COLLATE utf8mb4_unicode_ci,
  `post_modified` datetime NOT NULL,
  `post_modified_gmt` datetime NOT NULL,
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci,
  `post_parent` bigint(20) unsigned DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `menu_order` int(11) DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `comment_count` bigint(20) DEFAULT '0',
  `website_id` tinyint(4) NOT NULL,
  `image` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` date NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
from django.apps import AppConfig


class NcbiscrapperConfig(AppConfig):
    name = 'ncbiscrapper'

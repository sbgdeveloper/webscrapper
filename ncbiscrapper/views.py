from django.shortcuts import render

# Create your views here.

from django.http import HttpResponse
import csv
from datetime import date
from django.shortcuts import get_object_or_404, redirect, render
import os.path
from wikiscrapper.models import *
from bs4 import BeautifulSoup
import requests

def index(request):

	if request.method == 'POST':
		keyword = request.POST.get('keyword')

		term = keyword.replace(" ", "+")

		# https://www.ncbi.nlm.nih.gov/pmc/?term=blood+pressure

		listpage = requests.get("https://www.ncbi.nlm.nih.gov/pmc/?term="+term)

		listsoup = BeautifulSoup(listpage.content, 'html.parser')

		# alllinks = listsoup.find_all(class_="view")

		# for link in listsoup.find_all(class_="title"):
		# 	print(link)

		for linkdiv in listsoup.find_all(class_="title"):
			link = linkdiv.find(class_="view")
			articleurl = "https://www.ncbi.nlm.nih.gov"+link['href']
			page = requests.get(articleurl)
			soup = BeautifulSoup(page.content, 'html.parser')
			page_Title = soup.find(class_="content-title").get_text().encode('utf-8')
			complete_content = soup.find(class_="jig-ncbiinpagenav").encode('utf-8')
			images = soup.find_all(class_="tileshop")
			if len(images) > 0:
				page_image = "https://www.ncbi.nlm.nih.gov"+images[0]['src']
			else:
				page_image = ''
			base_path = "/var/www/html/webscrapper/storage/ncbi/"
			today = date.today()
			filename = today.strftime("%Y%m%d")
			filepath = base_path+filename+'.csv'


			if os.path.exists(filepath):
				row_list = [[keyword, page_Title, complete_content, page_image]]
				with open(filepath, 'a', newline='') as file:
					writer = csv.writer(file, quoting=csv.QUOTE_NONNUMERIC, delimiter=';')
					writer.writerows(row_list)
			else:
				row_list = [["Keyword", "Page Title", "Content", "Image"],[keyword, page_Title, complete_content, page_image]]
				with open(filepath, 'w', newline='') as file:
					writer = csv.writer(file, quoting=csv.QUOTE_NONNUMERIC, delimiter=';')
					writer.writerows(row_list)

				data = FilesModel()
				data.website_id = 2
				data.filename = filepath
				data.save()



		# return HttpResponse(page_image)

		# row_list = [["Keyword", "Page Title", "Content", "Image"],[keyword, page_Title, complete_content, page_image]]
		

		return redirect(request.META.get('HTTP_REFERER'))


		# return HttpResponse(wikipedia.summary(keyword))
	else:
		return render(request, "ncbiscrapper/templates/ncbiform.html")
	# return HttpResponse("Hello, world. You're at the polls index.")
from django.apps import AppConfig


class WikiscrapperConfig(AppConfig):
    name = 'wikiscrapper'

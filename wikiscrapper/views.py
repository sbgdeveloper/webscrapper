from django.shortcuts import render

# Create your views here.

from django.http import HttpResponse
import wikipedia
import csv
from datetime import date
from django.shortcuts import get_object_or_404, redirect, render
import os.path
from .models import *
from bs4 import BeautifulSoup
import requests

def index(request):

	if request.method == 'POST':
		keyword = request.POST.get('keyword')

		summary = wikipedia.summary(keyword).encode('utf-8')

		# page_Title = wikipedia.page(keyword)

		complete_page = wikipedia.page(keyword)
		complete_content = complete_page.content.encode('utf-8')
		page_Title = complete_page.title.encode('utf-8')

		if complete_page.images != None:
			page_image = complete_page.images[0].encode('utf-8')
		else:
			page_image = ''

		# page_image = wikipedia.page(keyword)

		base_path = "/var/www/html/webscrapper/storage/wikipedia/"

		today = date.today()

		# dd/mm/YY
		filename = today.strftime("%Y%m%d")

		filepath = base_path+filename+'.csv'


		if os.path.exists(filepath):
			row_list = [[keyword, page_Title, complete_content, page_image]]
			with open(filepath, 'a', newline='') as file:
				writer = csv.writer(file, quoting=csv.QUOTE_NONNUMERIC, delimiter=';')
				writer.writerows(row_list)
		else:
			row_list = [["Keyword", "Page Title", "Content", "Image"],[keyword, page_Title, complete_content, page_image]]
			with open(filepath, 'w', newline='') as file:
				writer = csv.writer(file, quoting=csv.QUOTE_NONNUMERIC, delimiter=';')
				writer.writerows(row_list)

			data = FilesModel()
			data.website_id = 1
			data.filename = filepath
			data.save()



		# return HttpResponse(page_image)

		# row_list = [["Keyword", "Page Title", "Content", "Image"],[keyword, page_Title, complete_content, page_image]]
		

		listpage = requests.get(complete_page.url)
		listsoup = BeautifulSoup(listpage.content, 'html.parser')

		linkdiv = listsoup.find(class_="div-col columns column-width")

		for link in linkdiv.find_all('a'):
			keyword = link.get_text()
			summary = wikipedia.summary(keyword).encode('utf-8')

			complete_page = wikipedia.page(keyword)
			complete_content = complete_page.content.encode('utf-8')
			page_Title = complete_page.title.encode('utf-8')

			if len(complete_page.images) > 0:
				page_image = complete_page.images[0].encode('utf-8')
			else:
				page_image = ''

			# page_image = wikipedia.page(keyword)

			base_path = "/var/www/html/webscrapper/storage/wikipedia/"

			today = date.today()

			# dd/mm/YY
			filename = today.strftime("%Y%m%d")

			filepath = base_path+filename+'.csv'


			if os.path.exists(filepath):
				row_list = [[keyword, page_Title, complete_content, page_image]]
				with open(filepath, 'a', newline='') as file:
					writer = csv.writer(file, quoting=csv.QUOTE_NONNUMERIC, delimiter=';')
					writer.writerows(row_list)
			else:
				row_list = [["Keyword", "Page Title", "Content", "Image"],[keyword, page_Title, complete_content, page_image]]
				with open(filepath, 'w', newline='') as file:
					writer = csv.writer(file, quoting=csv.QUOTE_NONNUMERIC, delimiter=';')
					writer.writerows(row_list)

				data = FilesModel()
				data.website_id = 1
				data.filename = filepath
				data.save()



		return redirect(request.META.get('HTTP_REFERER'))


		# return HttpResponse(wikipedia.summary(keyword))
	else:
		return render(request, "wikiscrapper/templates/wikipediaform.html")
	# return HttpResponse("Hello, world. You're at the polls index.")
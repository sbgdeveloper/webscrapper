import arrow
import binascii
import datetime
import os
import time
from django.core.cache import cache
from django.db import models
from django.utils.translation import ugettext_lazy as _
# Create your models here.


class FilesModel(models.Model):

    id = models.IntegerField(primary_key=True)
    website_id = models.IntegerField()
    filename = models.TextField()
    status = models.IntegerField()
    created_at = models.DateTimeField(_("Created on"), auto_now_add=True)
     
    class Meta:
        db_table = "files"
        def __str__(self):
            return self 